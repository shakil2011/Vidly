﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Vidly.Models;

namespace Vidly.Controllers
{
    using Vidly.ViewModels;

    public class CustomersController : Controller
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            
        }







        public ActionResult New()
        {
            var membershipTypes = this._context.MembershipTypes.ToList();

            var viewModel = new CustomerFormViewModel
            {
                Customer = new Customer(),
                MembershipTypes = membershipTypes
            };
            return View("CustomerForm",viewModel);  
        }





        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save (Customer customer)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new CustomerFormViewModel
                {
                    Customer = customer,
                    MembershipTypes = this._context.MembershipTypes.ToList()
                };
                return this.View("CustomerForm", viewModel);
            }


            if (customer.Id == 0)
            {
                this._context.Customers.Add(customer);
            }
            else
            {
                var customerInDb = this._context.Customers.Single(c => c.Id == customer.Id);

                customerInDb.Name = customer.Name;
                customerInDb.Birthdate = customer.Birthdate;
                customerInDb.MembershipTypeId = customer.MembershipTypeId;
                customerInDb.IsSubscribedToNewsLatter = customer.IsSubscribedToNewsLatter; 

            }
            
            this._context.SaveChanges();

            return RedirectToAction("Index", "Customers");
        }




        public ViewResult Index()
        {
            //var customers = _context.Customers.Include(c => c.MembershipType).ToList();

            //return View(customers);
            return View();
        }



        public ActionResult Details(int id)
        {
            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            return View(customer);
        }





        public ActionResult Edit(int id)
        {
            var customer = this._context.Customers.SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return this.HttpNotFound();

            var viewModel = new CustomerFormViewModel()
            {
                Customer = customer,
                MembershipTypes = this._context.MembershipTypes.ToList()
            };                   

            return this.View("CustomerForm" , viewModel);
        }
    }
}